import React, { Component } from "react";
import { HashRouter as Router, Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

// import components
import CategoriesList from "./components/categories/categories-list";
import LocationsList from "./components/locations/locations-list";



// router
class App extends Component {
  render() {
    return (
      <Router>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <Link to={"/"} className="navbar-brand">
            Home
          </Link>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/categories"} className="nav-link">
                Categories
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/locations"} className="nav-link">
                Locations
              </Link>
            </li>
          </div>
        </nav>
        <div className="container mt-3">
        <Switch>
            <Route exact path="/categories" component={CategoriesList} />
        </Switch>
        <Switch>
            <Route exact path="/locations" component={LocationsList} />
        </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
