import * as React from 'react';
import { useState } from 'react';
import ReactMapGL from 'react-map-gl';

const token = 'pk.eyJ1Ijoib21hci0xMjMiLCJhIjoiY2tzaWg0bWNnMjN2ZTJ2b2R0MWl4eDFoayJ9.8w3AvbS22J4JlPu_iJSvJA';

const maps = ({ location }) => {
  const [viewport, setViewport] = useState({
    width: 'auto', 
    height: 400,
    latitude: +location.latitude,
    longitude: +location.longitude,
    zoom: 8
  });

  return (
    <ReactMapGL
    mapboxApiAccessToken={token}
      {...viewport}
      onViewportChange={nextViewport => setViewport(nextViewport)}
    />
  );
}
export default maps;