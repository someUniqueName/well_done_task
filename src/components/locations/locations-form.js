import React, { useState } from "react";
import { connect } from "react-redux";
import {
    create, update
} from "../../redux/actions";

const locationForm = ({ data, setOpenForm, setDataSelected, categories, ...props }) => {
    const [values, setValues] = useState(data && data.id ? data : {
        name: '',
        address: '',
        longitude: '',
        latitude: '',
        category: '',
    });

    const storeData = (e) => {
        e.preventDefault();

        if (Object.keys(values).find(key => !values[key].trim())) {
            return alert('all fields are required');
        }

        if(!isLongitude(values.longitude)){
            return alert('longitude is invalid');
        }
            
        if(!isLatitude(values.latitude)){
            return alert('latitude is invalid');
        }

        props[data ? 'update' : 'create']('locations', { ...values, id: data && data.id });
        alert(data ? 'location updated' : 'location added!');
        closeForm();
    }

    const closeForm = () => {
        setOpenForm(false);
        setDataSelected(null);
    }

    const onChange = ({ target }) => {
        const { name, value } = target;
        setValues(state => {
            return { ...state, [name]: value }
        })
    }

    console.log(values)

    return (
        <>
            <form>
                <div className="form-group">
                    <label>Location Name</label>
                    <input type="text" className="form-control" name="name" placeholder="name" onChange={onChange} value={values.name} />
                    <input type="text" className="form-control" name="address" placeholder="address" onChange={onChange} value={values.address} />
                    <input type="text" className="form-control" name="longitude" placeholder="longitude" onChange={onChange} value={values.longitude} />
                    <input type="text" className="form-control" name="latitude" placeholder="latitude" onChange={onChange} value={values.latitude} />

                    <select name="category" value={values.category} onChange={onChange}>
                        <option value="">Select caregory</option>
                        {
                            categories.map((item, index) => {
                                return <option key={index} value={item.id} >{item.category}</option>
                            })
                        }
                    </select>


                </div>
                <button onClick={storeData} >{values.id ? 'Update' : 'Save'}</button>  <button onClick={closeForm} >Close</button>
            </form>
            <hr />
        </>
    )
};

const mapStateToProps = (state) => {
    return {
        categories: _.get(state, 'servicesReducer.categories', []),
    };
};

export default connect(mapStateToProps, {
    create,
    update,
})(locationForm);


function isLatitude(lat) {
    return isFinite(lat) && Math.abs(lat) <= 90;
}

function isLongitude(lng) {
    return isFinite(lng) && Math.abs(lng) <= 180;
}