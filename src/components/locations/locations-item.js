import React from "react";
import { connect } from "react-redux";
import {
    deleteOne
} from "../../redux/actions";
import _ from "lodash"
import Maps from '../map';

const locationItem = ({ item, setDataSelected, categories, ...props }) => {

    const deleteItem = (id) => {
        props.deleteOne('locations', id);
        alert('location deleted.');
    }


    return (
    <div style={{marginTop: '10px', marginBottom: '10px', background: 'antiquewhite', padding: '5px'}}>
        <p>name: {item.name}</p>
        <p>address: {item.address}</p>
        <p>coordinates: {item.coordinates}</p>
        <p>category: {_.get(categories.find(ele => ele.id === item.category), 'category', '')}</p>
        <button onClick={ () => setDataSelected(item) } >Update</button> 
        <button onClick={ () =>  deleteItem(item.id) } >Delete</button>
        <Maps location={item} />
    </div>
    )
}

const mapStateToProps = (state) => {
    return {
        categories: _.get(state, 'servicesReducer.categories', []),
    };
};

export default connect(mapStateToProps, {
    deleteOne,
})(locationItem);
