import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import {
  retrieveAll
} from "../../redux/actions";
import _ from 'lodash';
import LocationItem from './locations-item';
import LocationForm from './locations-form';


const locationsList = ({ locations, categories, ...props }) => {

  const [openForm, setOpenForm] = useState(false);
  const [dataSelected, setDataSelected] = useState(null);
  const [selectedCategory, setSelectedCategory] = useState('');


  useEffect(() => {
    props.retrieveAll('categories');
    props.retrieveAll('locations');
  }, []);


  return (
    <div>
      {
        openForm || dataSelected ? <LocationForm data={dataSelected} setOpenForm={setOpenForm} setDataSelected={setDataSelected} /> : null
      }


      {!openForm && !dataSelected ?
        <>
          <button onClick={() => {
            setOpenForm(true);
            setDataSelected(null);
          }} >
            {'Add Location'}
          </button>
          <br />
          <select name="category" value={selectedCategory} onChange={(e) => setSelectedCategory(e.target.value)}>
            <option value="">Select caregory</option>
            {
              categories.map((item, index) => {
                return <option key={index} value={item.id} >{item.category}</option>
              })
            }
          </select>
          {
            locations.filter(item => selectedCategory ? item.category === selectedCategory : item).sort((a, b) => (a.name < b.name ? -1 : a.name > b.name ? 1 : 0)).map((item, index) => {
              return <LocationItem key={index} item={item} setOpenForm={setOpenForm} setDataSelected={setDataSelected} />
            })
          }
        </>
        : null
      }
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    categories: _.get(state, 'servicesReducer.categories', []),
    locations: _.get(state, 'servicesReducer.locations', []),
  };
};

export default connect(mapStateToProps, {
  retrieveAll,
})(locationsList);