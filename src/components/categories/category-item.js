import React from "react";
import { connect } from "react-redux";
import {
    deleteOne
} from "../../redux/actions";

const categoryItem = ({ item, setDataSelected, ...props }) => {

    const deleteItem = (id) => {
        props.deleteOne('categories', id);
        alert('category deleted.');
    }

    return (
    <div style={{marginTop: '10px', marginBottom: '10px', background: 'antiquewhite', padding: '5px'}}>
        <p>Category name: {item.category}</p>
        <button onClick={ () => setDataSelected(item) } >Update</button> 
        <button onClick={ () =>  deleteItem(item.id) } >Delete</button>
    </div>
    )
}

const mapStateToProps = (state) => {
    return {
        categories: state.categories,
    };
};

export default connect(mapStateToProps, {
    deleteOne,
})(categoryItem);
