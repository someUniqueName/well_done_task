import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import {
  retrieveAll
} from "../../redux/actions";
import CategoryForm from './category-form';
import _ from 'lodash';
import CategoryItem from './category-item';


const categoriesList = ({ categories, ...props }) => {

  const [openForm, setOpenForm] = useState(false);
  const [dataSelected, setDataSelected] = useState(null);

  useEffect(() => {
    props.retrieveAll('categories');
  }, []);


  return (
    <div>
      {
        openForm || dataSelected ? <CategoryForm data={dataSelected} setOpenForm={setOpenForm} setDataSelected={setDataSelected} /> : null
      }

      {!openForm && !dataSelected ?
        <>
          <button onClick={() => {
            setOpenForm(true);
            setDataSelected(null);
          }} >
            {'Add category'}
          </button>
          {categories.sort((a, b) => (a.category < b.category ? -1 : a.category > b.category ? 1 : 0)).map((item, index) => {
            return <CategoryItem key={index} item={item} setOpenForm={setOpenForm} setDataSelected={setDataSelected} />
          })}
        </>
        : null
      }
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    categories: _.get(state, 'servicesReducer.categories', []),
  };
};

export default connect(mapStateToProps, {
  retrieveAll,
})(categoriesList);