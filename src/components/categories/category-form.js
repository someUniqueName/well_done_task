import React, { useState } from "react";
import { connect } from "react-redux";
import {
    create, update
} from "../../redux/actions";

const categoryForm = ({ data, setOpenForm, setDataSelected, ...props }) => {

    const [category, setCategory] = useState(data && data.category || '');

    const storeData = (e) => {
        e.preventDefault();
        const value = category.trim();
        if (!value) {
            return alert('enter valid name for category!');
        }
        props[data ? 'update' : 'create']('categories', { category, id: data && data.id });
        alert(data ? 'Category updated' : 'Category added!');
        closeForm();
    }

    const closeForm = () => {
        setOpenForm(false);
        setDataSelected(null);
    }

    return (
        <>
            <form>
                <div className="form-group">
                    <label>Category Name</label>
                    <input type="text" className="form-control" placeholder="Category" onChange={(e) => setCategory(e.target.value)} value={category} />
                </div>
                <button onClick={storeData} >{data ? 'Update' : 'Save'}</button>  <button onClick={closeForm} >Close</button>
            </form>
            <hr />
        </>
    )
};

const mapStateToProps = (state) => {
    return {
        categories: state.categories,
    };
};

export default connect(mapStateToProps, {
    create,
    update,
})(categoryForm);