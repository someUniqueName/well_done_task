import { get, store, generateUniqeID } from '../utils';

// data service to modify local storge

class locationsDataService {

  getAll(resource) {
    return { result: get(resource), resource };
  }

  get(resource, id) {
    const item = (get(resource)).find(ele => ele.id === id);
    return item;
  }

  create(resource, data) {
    const items = get(resource);
    items.push({ ...data, id: generateUniqeID()});
    store(resource, items);
  }

  update(resource, { id, ...data }) {
    const items = get(resource);
    const newItems = items.map(item => {
      if (item.id === id) {
        return { id, ...data };
      }
      return item;
    });
    store(resource, newItems);
  }

  delete(resource, id) {
    const items = get(resource);
    const newItems = items.filter(item => item.id !== id);
    store(resource, newItems);
  }

}

export default new locationsDataService();