import {
  CREATE,
  RETRIEVE_ALL,
  UPDATE,
  DELETE,
} from "../actions/types";

const initialState = {};

function tutorialReducer(data = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case CREATE:
    case RETRIEVE_ALL:
    case UPDATE:
    case DELETE:
      return { ...data, [payload.resource]: payload.result };

    default:
      return initialState;
  }
};

export default tutorialReducer;