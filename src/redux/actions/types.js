// types
export const CREATE = "CREATE";
export const RETRIEVE_ONE = "RETRIEVE_ONE";
export const RETRIEVE_ALL = "RETRIEVE_ALL";
export const UPDATE = "UPDATE";
export const DELETE = "DELETE";
