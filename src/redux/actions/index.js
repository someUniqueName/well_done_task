import {
  CREATE,
  RETRIEVE_ALL,
  RETRIEVE_ONE,
  UPDATE,
  DELETE,
} from "./types";

import DataService from "../../services";

export const create = (resource, data) => (dispatch) => {
  try {

    DataService.create(resource, data);
    const res = DataService.getAll(resource);

    dispatch({
      type: CREATE,
      payload: res,
    });

    return Promise.resolve(res);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const retrieveAll = (resource) => (dispatch) => {
  try {
    const res = DataService.getAll(resource);

    dispatch({
      type: RETRIEVE_ALL,
      payload: res,
    });

    return Promise.resolve(res);
  } catch (err) {
    console.log(err);
  }
};

export const retrieveOne = (resource, id) => (dispatch) => {
  try {

    const res = DataService.get(resource, id);
    dispatch({
      type: RETRIEVE_ONE,
      payload: res,
    });

    return Promise.resolve(res);
  } catch (err) {
    console.log(err);
  }
};

export const update = (resource, data) => (dispatch) => {
  try {

    DataService.update(resource, data);
    const res = DataService.getAll(resource);

    dispatch({
      type: UPDATE,
      payload: res,
    });

    return Promise.resolve(res);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const deleteOne = (resource, id) => (dispatch) => {
  try {
    DataService.delete(resource, id);
    if (resource === 'categories') {
      const locations = DataService.getAll('locations');
      locations.result.forEach(item => {
        if (item.category === id) {
          DataService.delete('locations', item.id);
        }
      });
      const newres = DataService.getAll('locations');

      dispatch({
        type: DELETE,
        payload: newres,
      });
    }
    const res = DataService.getAll(resource);

    dispatch({
      type: DELETE,
      payload: res,
    });

    return Promise.resolve(res);
  } catch (err) {
    console.log(err);
  }
};

