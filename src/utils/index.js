
// utility to get value from the local storge
export const get = ( key ) => {
    let keyValue = localStorage.getItem(key);
    if(keyValue){
        try{
            keyValue = JSON.parse(keyValue);
        }catch(error){
            // error no need to handle
        }finally{
            return keyValue
        }
    }
    return [];
}


// store value in local stoarge 
export const store = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
}

// generate random id
export const generateUniqeID = () => {
    return Math.random().toString().split('.')[1];
}